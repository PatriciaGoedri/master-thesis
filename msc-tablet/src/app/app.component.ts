import {Component, NgModule} from '@angular/core';
import { Router } from '@angular/router';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import {Scenario2Component} from "./components/scenario2/scenario2.component";
import {CommunicationService} from "./components/communication.service";



// @ts-ignore
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gaze';

  constructor(private router:Router,
              private communication:CommunicationService,
              ){

    this.communication.receiveData();






  }

  navigate() {
    //do your any operations
    this.router.navigate(["/switch"]);

  }
}


