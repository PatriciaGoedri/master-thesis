import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";



export interface DialogData {
  name: string;
}

@Component({
  selector: 'app-select-shape',
  templateUrl: './select-shape.component.html',
  styleUrls: ['./select-shape.component.css']
})
export class SelectShapeComponent implements OnInit {




  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openName() {

  }

  openItems() {
  }

  openDrinks() {

  }

  openDoors() {

  }

}

