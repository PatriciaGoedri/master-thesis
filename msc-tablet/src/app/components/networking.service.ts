import { Injectable, NgZone } from '@angular/core';
import { Socket } from 'ngx-socket-io';
//import _ from 'lodash';

const GROUP = 'default'; //SyncConfiguration script in unity

interface ModelMessage {
  command: 'modelUpdate' | 'modelDelete' | 'change';
  payload: any;
}

export interface ModelRegistration {
  name: string;
  onUpdate: (modelData: any) => void;
  onDelete: (modelData: any) => void;
  onData: (modelData: any) => void;
  registrationData?: any;
}

@Injectable({
  providedIn: 'root'
})
export class NetworkingService {

  constructor(
    public socket: Socket,
    private zone: NgZone) {

    // register group
    this.registerGroup();

      // for debugging
      (window as any).SyncService = this;
  }

  public registerGroup(): void {
    setTimeout(() => {
      if (this.socket.ioSocket.connected) {
        this.sendCommand("group", "set", { name: "TODO" });
      } else {
        this.registerGroup();
      }
    }, 100);
  }

  public sendCommand(channel: string, valueType: string, value: any): void {
    this.socket.emit(channel, {
      group: `GROUP_${GROUP}`,
      command: valueType,
      payload: value
    });
  }




  public registerModel(registration: ModelRegistration): Promise<void> {
    // receive updates
    this.socket.on(`${registration.name}Model`, (msg: ModelMessage) => {
      if (msg.command === 'modelUpdate') {
        registration.onUpdate(msg.payload);
      }
      if (msg.command === 'modelDelete') {
        registration.onDelete(msg.payload);
      }
      if (msg.command === 'change'){
        registration.onData(msg.payload);
      }

      this.socket.on("group", console.log)

      this.socket.ioSocket.onAny((n: any ,msg: any) => {

        console.log(n);
        registration.onData(msg.payload);


      })

    });

    // initial data fetch
    return new Promise<void>((resolve, reject) => {
      this.socket.once(`${registration.name}ModelInit`, (msg: ModelMessage) => {
        debugger
        for (const data of msg.payload) {
          registration.onUpdate(data);
        }
        resolve();
      });
      this.socket.emit(`${registration.name}Model`, { command: 'modelFetch', payload: registration.registrationData });
    });
  }

  public updateModel(name: string, model: any): void {
    this.socket.emit(`${name}Model`, {
      group: `GROUP_${GROUP}`,
      command: 'modelUpdate',
      payload: model
    });
  }

  public deleteModel(name: string, modelId: any): void {
    this.socket.emit(`${name}Model`, {
      group: `GROUP_${GROUP}`,
      command: 'modelDelete',
      payload: modelId
    });
  }
}
