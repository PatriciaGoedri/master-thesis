import { Injectable } from '@angular/core';

import {HttpClient, HttpClientModule} from "@angular/common/http";


import {Router} from '@angular/router';
import {ModelRegistration, NetworkingService} from "./networking.service";
import {Scenario2Component} from "./scenario2/scenario2.component";

export class Service {
  constructor(
    id: number
  ) {
  }


}
@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  active : boolean = false;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private networking: NetworkingService

  ) {

    this.registerModel().then(r => console.log(r))
  }


  public receiveData(){
    this.networking.socket.on("message", (value:any)=>this.changeLayout(value.message))

  }

  public async registerModel(){

    const model : ModelRegistration = {name: "default", onUpdate: console.log , onDelete:console.log, onData:this.changeLayout}

    await this.networking.registerModel(model);

    console.log(model);
  }

  changeLayout(val : any){


      this.active = val == 2;

  }

  SendData(number: string) {
    this.networking.socket.on("group", (value:any)=>console.log(value.payload))
    //console.log(number);

    this.networking.sendCommand("group", "string", number);
  }

}
