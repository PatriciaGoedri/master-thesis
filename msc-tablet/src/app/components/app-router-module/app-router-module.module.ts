import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Scenario2Component} from "../scenario2/scenario2.component";
import {Scenario3LessComponent} from "../scenario3-less/scenario3-less.component";
import {Scenario4ColorsComponent} from "../scenario4-colors/scenario4-colors.component";
// CLI imports router





const routes: Routes = [

  //normal scenario
  //##################################################

  { path: 'scenario2', component: Scenario2Component },

  { path: '', redirectTo: '/scenario2', pathMatch: 'full' }, //redirect to scenario1



  //##################################################

  //second scenario

/*
  { path: 'scenario3-less', component: Scenario3LessComponent},

  { path: '', redirectTo: '/scenario3-less', pathMatch: 'full' },

*/

  //third scenario (only colors)
/*
  { path: 'scenario4-colors', component: Scenario4ColorsComponent},

  { path: '', redirectTo: '/scenario4-colors', pathMatch: 'full' },
*/
];
// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModuleComponent {}




