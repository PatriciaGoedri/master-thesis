import { Component, OnInit } from '@angular/core';
import {CommunicationService} from "../communication.service";
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {DialogComponentComponent} from "../dialog-component/dialog-component.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {SelectColorComponent} from "../select-color/select-color.component";
import {SelectShapeComponent} from "../select-shape/select-shape.component";





@Component({
  selector: 'app-scenario2',
  templateUrl: './scenario2.component.html',
  styleUrls: ['./scenario2.component.css']
})

export class Scenario2Component implements OnInit {

  //active : boolean = false;

  setColorRed: boolean = false;

  setColorGreen: boolean = false;

  setColorPink: boolean = false;
  setColorOrange: boolean = false;

  setColorBlue: boolean = false;

  setShapeCube: boolean = false;

  setShapeCircle: boolean = false;

  setShapeCone: boolean = false;


  private color: any;
  private shape: any;







  constructor(
    public service:CommunicationService,

    public dialog: MatDialog



) { }

  ngOnInit(): void {
  }


  get active() {
    return this.service.active;
  }



/*
  testActive() {
    this.active = !this.active;
  }*/

  openDialog() {
    this.dialog.open(DialogComponentComponent);
  }

  openDialogColor() {


    const dialogRef = this.dialog.open(SelectColorComponent, {
      width: '250px',
      data: {value: this.color},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      this.color = result;
      console.log(result);

      switch(this.color) {
        case 'red':
          this.setColorRed = true;

          this.setColorOrange = false;
          this.setColorPink = false;
          this.setColorGreen = false;
          this.setColorBlue = false;
          break;
        case 'blue':
          this.setColorBlue = true;

          this.setColorOrange = false;
          this.setColorPink = false;
          this.setColorGreen = false;
          this.setColorRed = false;
          break;
        case 'green':
          this.setColorGreen = true;

          this.setColorOrange = false;
          this.setColorPink = false;
          this.setColorBlue = false;
          this.setColorRed = false;
          break;
        case 'pink':
          this.setColorPink = true;

          this.setColorOrange = false;
          this.setColorBlue = false;
          this.setColorGreen = false;
          this.setColorRed = false;
          break;
        case 'orange':
          this.setColorOrange = true;

          this.setColorBlue = false;
          this.setColorPink = false;
          this.setColorGreen = false;
          this.setColorRed = false;
      }

      this.service.SendData(result);
    });


  }

  openDialogItems() {

    const dialogRef = this.dialog.open(SelectShapeComponent, {
      width: '250px',
      data: {value: this.shape},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.shape = result;

      switch(this.shape) {
        case 'cube':
          this.setShapeCube = true;

          this.setShapeCircle = false;
          this.setShapeCone = false;

          break;
        case 'cone':
          this.setShapeCone = true;

          this.setShapeCircle = false;
          this.setShapeCube = false;
          break;
        case 'circle':
          this.setShapeCircle = true;

          this.setShapeCube = false;
          this.setShapeCone = false;
      }

      this.service.SendData(result);
    });


  }

  switchUI() {
    this.service.active = !this.service.active;
  }
}
