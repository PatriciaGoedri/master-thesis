import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select-color',
  templateUrl: './select-color.component.html',
  styleUrls: ['./select-color.component.css']
})
export class SelectColorComponent implements OnInit {

  constructor() { }

  setColorRed: boolean = false;

  setColorGreen: boolean = false;

  setColorPink: boolean = false;
  setColorOrange: boolean = false;

  setColorBlue: boolean = false;

  ngOnInit(): void {
  }

}
