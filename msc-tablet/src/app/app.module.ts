import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {AppComponent} from './app.component';

import {Scenario2Component} from "./components/scenario2/scenario2.component";
import {AppRoutingModuleComponent} from "./components/app-router-module/app-router-module.module";

import { HttpClientModule } from '@angular/common/http';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {NetworkingService} from "./components/networking.service";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//const config: SocketIoConfig = { url: 'paddi.oesterlin.dev:9001', options: {} }; //own server ip
//const config: SocketIoConfig = { url: 'smartmobility.hci.uni-konstanz.de:9001', options: {} }; //server for study
const config: SocketIoConfig = { url: 'lab.hci.uni-konstanz.de:9001', options: {} };

import {CommunicationService} from "./components/communication.service";

import {MatButtonModule} from "@angular/material/button";

import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import { DialogComponentComponent } from './components/dialog-component/dialog-component.component';
import { SelectShapeComponent } from './components/select-shape/select-shape.component';
import { SelectColorComponent } from './components/select-color/select-color.component';

import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatIconModule} from "@angular/material/icon";
import {MatPseudoCheckboxModule} from "@angular/material/core";
import {MatTableModule} from "@angular/material/table";
import { Scenario3LessComponent } from './components/scenario3-less/scenario3-less.component';
import { Scenario4ColorsComponent } from './components/scenario4-colors/scenario4-colors.component';




// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    Scenario2Component,
    DialogComponentComponent,
    SelectShapeComponent,
    SelectColorComponent,
    Scenario3LessComponent,
    Scenario4ColorsComponent

  ],

  imports: [
    BrowserModule,
    AppRoutingModuleComponent,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatIconModule,
    MatPseudoCheckboxModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule{}
