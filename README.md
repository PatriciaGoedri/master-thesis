# Gaze-Adaptive Menus for Hybrd User Interfaces in AR

Nowadays, menus are an essential part in user interfaces. Especially, in hybrid
user interfaces that combine traditional devices with novel Augmented
Reality (AR) Head-Mounted Displays (HMD) to compensate disadvantages of
gesture interaction. This new option for interaction allows the user to interact with
the mobile device eyes-free to focus completely on the content of the
AR environment. In this thesis, an adaptive menu that changes depending on the user's interaction was designed, implemented, and evaluated.


## Hardware

- Microsoft HoloLens2
- Apple iPad Pro 2017

## Software

**WebServer Connection:** Using TypeScript, a WebSocket was set up to enable communication between the tablet and the Hololens 2. Accordingly, the WebSocket server enables communication between a Unity C\# client and the tablet client. For the WebSocket client in Unity, the NuGet Unity plugin was used

**AR Content:** Within the Hololens 2, the gaze is to be captured. A distinction can be made between head gaze and eye gaze. The first iteration only used the head gauze by default. This was triggered because OpenXR requires an additional setting within the EyeGazeDataProvider. After the Eye Gaze Interaction Profile is configured, the Gaze Input can be accessed using the IMixedRealityEyeGazeProvider. Thus, the current gaze can be visualized using a cursor to provide interactive feedback to the user.

In order for eye tracking to work accurately, each user must go through an eye tracking user calibration. This calibration process occurs at the beginning of the project each time a new user uses the project. This ensures a valid evaluation of the results within the study.

By using eye gaze movement paired with hand tracking the tablet state can be tracked. Therefore, a script is triggered as soon as the eye gaze is out of the current field of view. To overcome that people are moving their eyes too fast, the script will also be enabled when the users hands on the tablet are tracked by the camera of the Hololens 2. Due to this, EnableSkript() sends a message to the WebSocket, which initiates a change of the tablet interface.

Messages from the tablet can be observed and processed with the help of the Unity C# client. Depending on the message, an action is performed. These actions differ among the methods CreateObject(), MoveTarget(), LinkTarget() and DeleteTarget().

**Tablet:** 4 different user interfaces (No Transition, Small Transition, Medium Transition, Full Transition) were implemented using Angular.



# Usability study

The study conducted for the master thesis will be a controlled lab experiment. The goal of this study is to achieve a better understanding of the impact of a gaze-adaptive menu for hybrid user
interfaces in AR. 

Based on the related work, following research questions were elaborated. Besides, hypotheses were derived to extend each research question.

**RQ1:** How does the use of gaze-adaptive menus influence the performance in AR?

    H1: Gaze Adaptive menus improve the time to perform an operation.

    H2: Gaze Adaptive menus reduce the effort required to complete a task.

**RQ2:** How does the gaze-adaptive menu support a natural interaction with the AR content?

**RQ3:** Which condition of the Hybrid User Interface do users prefer?




