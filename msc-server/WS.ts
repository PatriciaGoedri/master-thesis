import { randomBytes } from "crypto";

import http from 'http';
import { Server } from "socket.io";
import express from 'express';
import cors from 'cors';
import WebSocket from "ws";

const app = express();
app.use(cors({}));

const webServer = http.createServer(app);
const io = new Server(webServer, {
  allowEIO3: true,
  cors: {
    origin: 'http://smartmobility.hci.uni-konstanz.de:4200', // set to the url the angular site is hosted on
    methods: ["GET", "POST"],
    credentials: true,
  },
});

const room = randomBytes(5).toString("hex");

app.get("/", (_req, res) => {
  const html = `
  <html>
  <body>
    <script>
    webSocket = new WebSocket("ws://smartmobility.hci.uni-konstanz.de:9002");
    webSocket.onopen = function (event) {
      webSocket.send("Here's some text that the server is urgently awaiting!");
    };
    </script>
    test
  </body>
  </html>
  `
  res.send(html)
})

const wss = new WebSocket.Server({ port: 9002 }, () => {
  console.log('server started')
})

const allConnections = [];

wss.on('connection', function connection(ws) {
  allConnections.push(ws);
  console.log("new unity connection")

  ws.on('message', (buffer: Buffer) => {
    const data = buffer.toString('utf-8');
    console.log("sending to website:", data)
    io.in(room).emit('message', {
      message: parseInt(data)
    });
  });
})

io.on('connection', (socket) => {
  console.log('website connected');
  socket.join(room);
  socket.onAny((eventName, args) => {
    console.log(eventName, args.message);
    allConnections.forEach(c => c.send(args.message ?? args.payload ?? ""))
  });
});

webServer.listen(9001, () => {
  console.log('web version listening on http://localhost:9001');
});

wss.on('listening', () => {
  console.log('unity version listening on http://localhost:9002')
})