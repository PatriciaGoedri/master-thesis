using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineControl : MonoBehaviour
{
    public MeshOutline outline;
    public Color color;

    // Start is called before the first frame update
    void Start()
    {
        outline = GetComponent<MeshOutline>();
    }

    public void UpdateOutline()
    {
        outline = GetComponent<MeshOutline>();
    }

    private void Update()
    {
        this.gameObject.GetComponent<Renderer>().material.color = color;
    }

    public void HiglightTarget()
    {
      

        outline.enabled = true;
    }

    public void DeselectTarget()
    {
       
        outline.enabled = false;

        
    }
}
