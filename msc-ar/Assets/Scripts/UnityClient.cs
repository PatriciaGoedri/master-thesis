using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking;
using UnityEngine.EventSystems;

using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using UnityEngine;
using WebSocketSharp;
using UnityEngine.UI;
using System;

namespace Microsoft.MixedReality.Toolkit.Input{
public class UnityClient : MonoBehaviour , IMixedRealitySourceStateHandler


    {
    public string server = "servername";
    WebSocket ws;
    public string msg;
    public TabletInteraction interaction;

    public GameObject currentTarget;

    public GameObject startTarget;

    public GameObject secondTarget;

    public bool enable = false;

    public Transform cursor;

    

    public bool startTargetEnable = false;

    public bool isEyetrackingValid;

    public bool startMovement = false;

    public string color;

    public string shape;

    public bool bool_shape = false;

    public TextMesh consoleText;

    public Handedness myHandedness;

        public GameObject Ltarget1;

        public GameObject Ltarget2;


        public AudioSource audioSource;
        public AudioClip impact;
        public bool hasPlayed = false;

        public MeshOutline outline;
        public float counterE = 0;
        public float counterD = 0;


        private void Awake()
    {

      
        

        Debug.Log("skript started");

            ws = new WebSocket("ws://smartmobility.hci.uni-konstanz.de:8002");


            ws.Connect();
            ws.OnMessage += (sender, e) =>
            {
                msg = e.Data;
            };

            /*
            ws.OnOpen += (sender, e) =>
            {
                Debug.Log("OnOpen");
                consoleText.text = "onOpen";
             
            };
            ws.OnError += (sender, e) =>
            {
                Debug.Log("OnError: " + e.Message);
                consoleText.text = "Error" + e.Message;
            };*/




        }

        public bool link;

        private void Start()
        {

            link = false;

            CoreServices.InputSystem?.RegisterHandler<IMixedRealityFocusHandler>(this);

            

            
    }

        private void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            GameObject line = new GameObject();
            line.transform.position = start;
            line.AddComponent<LineRenderer>();
            LineRenderer renderer = line.GetComponent<LineRenderer>();
            renderer.material = new Material(Shader.Find("Standard"));
            renderer.material.SetColor("_Color", color);
            renderer.startWidth = 0.1f;
            renderer.endWidth = 0.1f;
            renderer.SetPosition(0, start);
            renderer.SetPosition(1, end);

        }

        private void OnEnable()
        {
            // Instruct Input System that we would like to receive all input events of type
            // IMixedRealitySourceStateHandler and IMixedRealityHandJointHandler
            CoreServices.InputSystem?.RegisterHandler<IMixedRealitySourceStateHandler>(this);
            
        }

        private void OnDisable()
        {
            // This component is being destroyed
            // Instruct the Input System to disregard us for input event handling
            CoreServices.InputSystem?.UnregisterHandler<IMixedRealitySourceStateHandler>(this);
            
        }


        //hand tracking
        
        public void OnSourceDetected(SourceStateEventData eventData)
        {
            var hand = eventData.Controller as IMixedRealityHand;
            if (hand != null)
            {
                //EnableSkript();
            } 
     
        }

        
        public void OnSourceLost(SourceStateEventData eventData)
        {
            var hand = eventData.Controller as IMixedRealityHand;

            // Only react to articulated hand input sources
            if (hand != null)
            {
                Debug.Log("Source lost: " + hand.ControllerHandedness);

               // DisableSkript();
            }

            
        } 





        public void DisableSkript(){

        enable = false;
        Debug.Log("Skript Disabled");
        string number = "1";

        ws.Send(number);

            counterD += 1;
    
                
    }

    

     public void EnableSkript(){

            enable = true;
            string number = "2";

            
         

            if (enable) {
                
                Debug.Log("Skript Enabled");
          
                ws.Send(number);

                counterE += 1;


                enable = false;

                



            }
        
        
    }

        public void SaveIntoJson()
        {
            string data = JsonUtility.ToJson(counterE);
            System.IO.File.WriteAllText(Application.persistentDataPath + "/DisableEye.json", data);
        }











        //set the focussed target
        public void LogCurrentGazeTarget()
{
            if (CoreServices.InputSystem.EyeGazeProvider.GazeTarget)
            {
                Debug.Log("User gaze is currently over game object: "
                    + CoreServices.InputSystem.EyeGazeProvider.GazeTarget);

                currentTarget = CoreServices.InputSystem.EyeGazeProvider.GazeTarget;

                //HiglightTarget(currentTarget);
            } else
            {
               // DeselectTarget(currentTarget);
            }
}



        Vector3 previousGazeDir;
        float directionZ;

        public GameObject target1;
        public GameObject target2;


        public LineRenderer lineRenderer;

   

        private float defaultDistanceInMeters = 2f;

        public void GetTarget(GameObject target)
        {
            currentTarget = target;
        }

        public void Update(){

            
            

            if (CoreServices.InputSystem.EyeGazeProvider.GazeTarget)
            {
                currentTarget = CoreServices.InputSystem.EyeGazeProvider.GazeTarget;

            }


            isEyetrackingValid = CoreServices.InputSystem.EyeGazeProvider.IsEyeTrackingDataValid;

        Debug.Log(isEyetrackingValid);

         float newDirectionZ = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.z;

            if (directionZ != 0)
            {
                float gazeDif = directionZ - newDirectionZ;

                //ws.Send("DIF:" + gazeAngle.ToString());
                /*
                if (gazeDif > -0.3f)
                {
                    ws.Send("DIF Enable:" + gazeDif.ToString());
                    EnableSkript();
                }
                if (gazeDif > 0f)
                {
                    ws.Send("DIF Disable:" + gazeDif.ToString());
                    DisableSkript();
                }*/
            }

            directionZ = newDirectionZ;





            Vector3 newGazeDir = CoreServices.InputSystem.EyeGazeProvider.GazeDirection;



            if (previousGazeDir != Vector3.zero)
            {
                float gazeAngle = Vector3.Angle(previousGazeDir, newGazeDir);

               // ws.Send("ANGLE:" + gazeAngle.ToString());
            }

            previousGazeDir = newGazeDir;

            Vector3 eyeForward = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
                CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 1f;

            Vector3 head = Camera.main.transform.position;

            if (CoreServices.InputSystem.EyeGazeProvider.IsEyeTrackingEnabledAndValid)
            {
                

                float gazOx = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.x;
                float gazOy = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.y;
                float gazOz = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.z;

                Vector3 gazeO = new Vector3(gazOx, gazOy, gazOz);

                float dirOx = CoreServices.InputSystem.EyeGazeProvider.GazeDirection.x;
                float dirOy = CoreServices.InputSystem.EyeGazeProvider.GazeDirection.y;
                float dirOz = CoreServices.InputSystem.EyeGazeProvider.GazeDirection.z;

  
                
                if( dirOy < -0.7f)
                {
                   // ws.Send("DIF Enable:" + dirOy.ToString());
                   EnableSkript();
                }
                
                if(dirOy >= (head.y - 0.1f))
                {
                   // ws.Send("DIF disable:" + dirOy.ToString());
                    DisableSkript();
                }



                Vector3 dir = new Vector3(dirOx, dirOy, dirOz);


               
                            

                

                

              

            }

           
        

        if(msg == "cone"){
                this.shape = "cone";
            bool_shape = true;

                



            }

        if(msg == "circle"){
                this.shape = "circle";
            bool_shape = true;
        }

        if(msg == "cube"){
                
                this.shape = "cube";
            bool_shape = true;
        }

        if(msg == "red"){
                this.color = "red";
            msg = null;
        }

        if(msg == "blue"){
                this.color = "blue";
            msg = null;
        }

        if(msg == "green"){
               
                this.color = "green";
            msg = null;
        }

        if(msg == "pink"){
                
                this.color = "pink";
            msg = null;
        }

        if(msg == "orange"){
                
                this.color = "orange";
            msg = null;
        }

    
    

        if(ws == null)
        {
            return;
        }

            if (msg == "sound")
            {

                if (hasPlayed == false)
                {
                    hasPlayed = true;

                    audioSource.Play();
                }

                
                msg = null;

                hasPlayed = false;

            }



                if (msg == "create")
        {


            audioSource.Play();

            interaction.CreateCube(shape, color);
            msg = null;

             
             
        }


         
        

            if (msg == "link" && link == false)
            {
                audioSource.Play();
                startTarget = CoreServices.InputSystem.EyeGazeProvider.GazeTarget;

                startTargetEnable = true;

               // startTarget.GetComponent<Renderer>().material.color = Color.blue;

                

                msg = null;
                link = true;


            }

            if (msg == "link" && link == true)
            {
                audioSource.Play();
                secondTarget = CoreServices.InputSystem.EyeGazeProvider.GazeTarget;
                

                //secondTarget.GetComponent<Renderer>().material.color = Color.red;

                interaction.LinkCubes(startTarget, secondTarget);

                msg = null;

                link = false;
            }

                
                  
               

          

            if (msg == "moveStart")
        {
                if (hasPlayed == false)
                {
                    hasPlayed = true;

                    audioSource.Play();
                }

                Debug.Log("move target");
            interaction.MoveTarget(currentTarget);

        }

        if(msg == "moveStop")
        {
                hasPlayed = false;

                interaction.StopMovement(currentTarget);
        }

        if(msg == "delete")
        {
                audioSource.Play();
                Debug.Log("Target Deleted");
            interaction.DeleteTarget(currentTarget);
            msg = null;
        }

        
            
        
    }
    
}
}
