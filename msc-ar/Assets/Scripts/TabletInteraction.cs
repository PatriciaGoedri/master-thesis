using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;

using JetBrains.Annotations;
using UnityEngine.AI;

namespace Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking{
public class TabletInteraction : MonoBehaviour
{

     public GameObject cube;
     public GameObject cone;
     public GameObject circle;
     private float defaultDistanceInMeters = 2f;
        public Material material;
    
     public GameObject cursor;

        public LineRenderer LineRenderer;
        public Transform TransformOne;
        public Transform TransformTwo;

      




        public void Start()
        {


            LineRenderer.GetComponent<LineRenderer>().enabled = false;
            LineRenderer.startColor = Color.red;
            LineRenderer.endColor = Color.red;

            // set width of the renderer
            LineRenderer.startWidth = 0.05f;
            LineRenderer.endWidth = 0.05f;

            // set the position
            LineRenderer.SetPosition(0, TransformOne.position);
            LineRenderer.SetPosition(1, TransformTwo.position);
        }





        public void CreateCube(string shape, string msg)
    {
        

        cursor = CoreServices.InputSystem.EyeGazeProvider.GazeTarget;

        
        

        if(shape == "cube"){

                GameObject cubeClone = Instantiate(cube, CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
                CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 1f , Quaternion.identity);

               

                OutlineControl control = cubeClone.AddComponent<OutlineControl>();
                

                if (msg == "red"){
            cubeClone.GetComponent<Renderer>().material.color = Color.red;
                    control.color = Color.red;

                }

            if(msg == "blue"){
            cubeClone.GetComponent<Renderer>().material.color = Color.blue;
                    control.color = Color.blue;

                }

             if(msg == "green"){
            cubeClone.GetComponent<Renderer>().material.color = Color.green;
                    control.color = Color.green;

                }

            if(msg == "pink"){
                cubeClone.GetComponent<Renderer>().material.color = Color.white;
                    control.color = Color.white;

                }

            if(msg == "orange"){
                cubeClone.GetComponent<Renderer>().material.color = Color.yellow;
                    control.color = Color.yellow;

                }

                

        } 

        if(shape == "cone"){

            GameObject coneClone = Instantiate(cone, CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
        CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 1f , Quaternion.identity);

                OutlineControl control = coneClone.AddComponent<OutlineControl>();


                if (msg == "red"){
            coneClone.GetComponent<Renderer>().material.color = Color.red;
                    control.color = Color.red;

                }

        if(msg == "blue"){
            coneClone.GetComponent<Renderer>().material.color = Color.blue;

                    control.color = Color.blue;

                }

        if(msg == "green"){
            coneClone.GetComponent<Renderer>().material.color = Color.green;

                    control.color = Color.green;
                }

        if(msg == "pink"){
            coneClone.GetComponent<Renderer>().material.color = Color.white;

                    control.color = Color.white;
                }

        if(msg == "orange"){
            coneClone.GetComponent<Renderer>().material.color = Color.yellow;
                    control.color = Color.yellow;
                }

        }

        if(shape == "circle"){

            GameObject circleClone = Instantiate(circle, CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
        CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 1f , Quaternion.identity);

                OutlineControl control = circleClone.AddComponent<OutlineControl>();

                if (msg == "red"){
            circleClone.GetComponent<Renderer>().material.color = Color.red;
                    control.color = Color.red;
                }

        if(msg == "blue"){
            circleClone.GetComponent<Renderer>().material.color = Color.blue;
                    control.color = Color.blue;
                }

        if(msg == "green"){
            circleClone.GetComponent<Renderer>().material.color = Color.green;
                    control.color = Color.green;
                }

        if(msg == "pink"){
            circleClone.GetComponent<Renderer>().material.color = Color.white;
                    control.color = Color.white;
                }

        if(msg == "orange"){
            circleClone.GetComponent<Renderer>().material.color = Color.yellow;
                    control.color = Color.yellow;
                }
        

        }

        


        
        



        //Instantiate(cube, new Vector3(0, 0, 0), Quaternion.identity);

    }


        public LineRenderer lineRend;

        public void LinkCubes(GameObject startTarget, GameObject secondTarget)
    {
            TransformOne = startTarget.transform;

            TransformTwo = secondTarget.transform;


            //lineRend.positionCount = 2;




            DrawLineBetweenObjects(TransformOne, TransformTwo);

            LineRenderer.enabled = true;

            /*
            lineRend.enabled = true;

            

            lineRend.startWidth = .45f;
            lineRend.endWidth = .45f;



            Transform first = startTarget.transform;
            Transform second = secondTarget.transform;

            DrawLineBetweenObjects(first, second);*/



        }

        void DrawLineBetweenObjects(Transform firstT, Transform secondT)
        {

            LineRenderer.startColor = Color.red;
            LineRenderer.endColor = Color.red;

            LineRenderer.startWidth = 0.05f;
            LineRenderer.endWidth = 0.05f;
            // Set the positions of the LineRenderer
            LineRenderer.SetPositions(new Vector3[] { firstT.position, secondT.position });
        }


        public GameObject target1;
        public GameObject target2;
       
       




        private Vector3 offsetToCamera = Vector3.zero;
       
        public void MoveTarget(GameObject selectedCube)
    {

          



            
            selectedCube.AddComponent<MoveObjByEyeGaze>().enabled = true;


            selectedCube.transform.position = Vector3.MoveTowards(selectedCube.transform.position, CoreServices.InputSystem.GazeProvider.GazeOrigin + CoreServices.InputSystem.GazeProvider.GazeDirection.normalized, .05f);





            // eye gaze movement    
            //   selectedCube.transform.position = Vector3.MoveTowards(selectedCube.transform.position, CoreServices.InputSystem.GazeProvider.GazeOrigin + CoreServices.InputSystem.GazeProvider.GazeDirection.normalized * 1f , .03f);

            // selectedCube.transform.position = Vector3.MoveTowards(selectedCube.transform.position, Camera.main.transform.position * 2 + Camera.main.transform.position.normalized * 1f, .03f);

            //   selectedCube.transform.position = CameraCache.Main.transform.position + offsetToCamera;
            //  selectedCube.transform.rotation = CameraCache.Main.transform.rotation;


        }

        public void StopMovement(GameObject selectedCube)
    {


            selectedCube.AddComponent<MoveObjByEyeGaze>().enabled = false;
            selectedCube.transform.position = selectedCube.transform.position;

    }



    public void DeleteTarget(GameObject selectedCube) 
    {
        Destroy(selectedCube);

    }



        public Material outlineMaterial;
        public MeshOutline outline;

        public void HiglightTarget(GameObject currentTarget)
        {

            //currentTarget.GetComponent<Renderer>().material.color = Color.blue;


            outline = currentTarget.GetComponent<MeshOutline>();
            //outline.OutlineMaterial = outlineMaterial;
            //outline.OutlineWidth = 0.002f;

            //Color changes
            //outline.OutlineMaterial.color = new Color(0.0f, 0.65f, 1.0f);


            outline.enabled = true;
        }

        public void DeselectTarget(GameObject currentTarget)
        {

            //Destroy(currentTarget.GetComponent<MeshOutline>());
            
            outline = currentTarget.GetComponent<MeshOutline>();

            outline.enabled = false;





            // currentTarget.GetComponent<Renderer>().material.color = Color.red;


        }
    }
}