using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities;


namespace Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking
{
    public class StartTask : MonoBehaviour
    {

        public GameObject cube;
        public GameObject cone;
        public GameObject circle;

        public GameObject task;

        public GameObject AreaCube;

        public GameObject AreaCone;

        public GameObject AreaCircle;

        GameObject cubeClone1;
        GameObject cubeClone2;
        GameObject cubeClone3;
        GameObject coneClone1;
        GameObject circleClone1;


        public Material material;


        // Start is called before the first frame update
        void Start()
        {
            float gazOx = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.x + 2f;
            float gazOy = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.y + .5f;
            float gazOz = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.z + 1f;


            float taskOy = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin.y + .5f;

            Vector3 eyeForward = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
                CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 1f;






            GameObject cubeClone1 = Instantiate(cube, eyeForward + new Vector3(.45f, -.095f) , Quaternion.identity);

            OutlineControl control = cubeClone1.AddComponent<OutlineControl>();


            GameObject cubeClone2 = Instantiate(cube, eyeForward + new Vector3(-.175f, -0.085f), Quaternion.identity);

            OutlineControl control2 = cubeClone2.AddComponent<OutlineControl>();

            GameObject cubeClone3 = Instantiate(cube, eyeForward + new Vector3(-.15f, -0.2f), Quaternion.identity);

            OutlineControl control3 = cubeClone3.AddComponent<OutlineControl>();

            GameObject coneClone1 = Instantiate(cone, eyeForward + new Vector3(0.3f, -0.45f), Quaternion.identity);

            OutlineControl control4 = coneClone1.AddComponent<OutlineControl>();

            GameObject circleClone1 = Instantiate(circle, eyeForward + new Vector3(0.2f, -0.06f), Quaternion.identity);

            OutlineControl control5 = circleClone1.AddComponent<OutlineControl>();



            GameObject areaCubeClone = Instantiate(AreaCube, eyeForward + new Vector3(0, .1f), Quaternion.identity);

            GameObject areaConeClone = Instantiate(AreaCone, eyeForward + new Vector3(-.3f, .1f), Quaternion.identity);

            GameObject areaCircleClone = Instantiate(AreaCircle, eyeForward + new Vector3(.3f, .1f), Quaternion.identity);

            cubeClone1.GetComponent<Renderer>().material.color = Color.red;
            control.color = Color.red;

            cubeClone2.GetComponent<Renderer>().material.color = Color.yellow;
            control2.color = Color.yellow;

            cubeClone3.GetComponent<Renderer>().material.color = Color.white;

            control3.color = Color.white;

            coneClone1.GetComponent<Renderer>().material.color = Color.white;

            control4.color = Color.white;

            circleClone1.GetComponent<Renderer>().material.color = Color.green;

            control5.color = Color.green;






        }

        // Update is called once per frame
        void Update()
        {
            cubeClone1.GetComponent<Renderer>().material.color = Color.red;

            cubeClone2.GetComponent<Renderer>().material.color = Color.yellow;

            cubeClone3.GetComponent<Renderer>().material.color = Color.white;

            coneClone1.GetComponent<Renderer>().material.color = Color.white;

            circleClone1.GetComponent<Renderer>().material.color = Color.green;

        }
    }
}
